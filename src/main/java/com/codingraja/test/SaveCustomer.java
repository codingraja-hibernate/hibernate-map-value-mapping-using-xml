package com.codingraja.test;

import java.util.HashMap;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.codingraja.domain.Product;

public class SaveCustomer {

	public static void main(String[] args) {
		
		Configuration configuration = new Configuration();
		configuration.configure("hibernate.cfg.xml");
		
		SessionFactory factory = configuration.buildSessionFactory();
		
		Map<String,String> props = new HashMap<String, String>();
		props.put("ram", "8GB");
		props.put("memory", "128GB");
		props.put("processor", "i5");
		
		Product product = new Product("MacBook", "MacBook Air", "Apple", 75000.0, props);
		
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(product);
		transaction.commit();
		session.close();
		
		System.out.println("Product has been saved successfully!");

	}

}
